import { get_operating_system, get_device, set_device_simulated_dimensions } from "./device-info/device-and-platform";
export { get_operating_system, get_device, set_device_simulated_dimensions};