// Detect operating system
const get_operating_system = () => {
      let userAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (/windows phone/i.test(userAgent)) return 'windows-phone';
      if (/android/i.test(userAgent)) return 'android';
      if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) return 'ios';
      if (!((typeof window.orientation !== 'undefined') || (navigator.userAgent.indexOf('IEMobile') !== -1))) return 'web';
      else return 'unknown'
};

// Get device media (only by media width size)
const get_device = () => {
      let device = '';
      if (window.innerWidth <= 768) device = 'mobile';
      else if (window.innerWidth > 768 && window.innerWidth <= 992) device = 'tablet';
      else device = 'desktop';
      return device
};

// set simulated vh
const set_device_simulated_dimensions = () => {
      let vh = window.innerHeight * 0.01 ,vw = window.innerWidth * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
      document.documentElement.style.setProperty('--vw', `${vw}px`)
};

export {get_operating_system, get_device, set_device_simulated_dimensions}
