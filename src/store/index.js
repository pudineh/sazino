import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
      state: {
            device: ''
      },
      mutations: {
            setDeviceType(state, device) {
                  state.device = device;
            }
      }
})

//import Vue from "vue";
//import Vuex from "vuex";

//Vue.use(Vuex);

//export default new Vuex.Store({
//      state: {
//            device: ''
//      },
//      mutations: {
//            setDevice(state, device) {
//                  state.device = device;
//            }
//      },
//      actions: {
//            deviceSetter({ commit }, device) {
//                  commit('setTimer', device)
//            }
//      }
//})