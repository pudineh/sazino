const path = require('path');  

module.exports = {
      chainWebpack: config => {
            config
                  .plugin('html')
                  .tap(args => {
                        args[0].title = "گروه فنی مهندسی سازینو";
                        return args;
                  })
      },
      configureWebpack: {
            resolve: {
                  alias: {
                        '@': path.resolve(__dirname, 'src')
                  },
            },
      },
      css: {
            loaderOptions: {
                  sass: {
                        sassOptions: {
                              includePaths: [
                                    path.resolve(__dirname, 'src')
                              ],
                              indentedSyntax: true,
                        },
                        additionalData : '@import "@/scss/main.scss"',
                  }
            }
      }
}